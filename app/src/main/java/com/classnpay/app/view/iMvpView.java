package com.classnpay.app.view;

import android.content.Context;

public interface iMvpView {
    Context getContext();
}
