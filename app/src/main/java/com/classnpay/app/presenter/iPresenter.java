package com.classnpay.app.presenter;

public interface iPresenter<V> {

    void attachView(V view);

    void detachView();
}
