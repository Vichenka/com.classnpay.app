package com.classnpay.app.presenter;

import com.classnpay.app.view.iMainMvpView;

public class MainPresenter implements iPresenter<iMainMvpView> {

    private iMainMvpView mainMvpView;

    @Override
    public void attachView(iMainMvpView view) {
        this.mainMvpView = view;
    }

    @Override
    public void detachView() {
        this.mainMvpView = null;
    }
}
